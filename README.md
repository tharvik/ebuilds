# Cam's Ebuild Repository
Various gentoo ebuilds I use. These programs are stable™, and may be outdated
from time to time. Submit an issue/bug report in [this repository](https://gitlab.com/camconn/ebuilds)
and I'll gladly update the appropriate ebuild.

Use at your own risk. This repository is licensed under the GNU Public License,
Version 3 or any later version at your choosing.  Please refer to `LICENSE` for
the full details of this license.

If you want to add this overlay via layman, type
```
layman -o https://gitlab.com/camconn/ebuilds/raw/master/profiles/camconn.xml -f -a camconn

```

# Packages
- app-text/urlscan: Replacement for urlview and extract\_url.pl
	- https://github.com/firecat53/urlscan
	- It's smart enough to correctly parse MIME encoded messages
	- Useful with something like [mutt-wizard](https://github.com/LukeSmithxyz/mutt-wizard)
- dev-lang/ziglang: The Zig programming language
	- robust, optimal, and clear programming language
	- https://ziglang.org
