# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit git-r3 cmake-utils

DESCRIPTION="The Zig Programming Language"
HOMEPAGE="https://ziglang.org"
EGIT_REPO_URI="https://github.com/ziglang/zig"

if [[ ${PV} == *9999 ]];then
	EGIT_COMMIT="HEAD"
else
	EGIT_COMMIT="${PV}"
fi

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

DEPEND="
	>=dev-util/cmake-2.8.5:=
	sys-devel/llvm:7=
	sys-devel/clang:7=
	sys-devel/gcc
"
RDEPEND=""

src_configure() {
	local mycmakeargs=(
		-DCLANG_INCLUDE_DIRS="/usr/lib64/llvm/7/include"
	)

	cmake-utils_src_configure
}

